<?php
/**
 * Plugin Name: Chinh's Plugin
 * Version: 1.0
 * Description: This is custom Widgets
 * Author: Mr Chinh
 * Author URI: //facebook.com/chinhphan099
*/

/**
 * Tạo class custom_Widget
 */
class custom_Widget extends WP_Widget {

  /**
  * Thiết lập widget: đặt tên, base ID
  */
  function __construct() {
    parent::__construct (
      'custom_widget', // id của widget
      'Custom Widget', // tên của widget
      array(
        'description' => 'First Wiget' // mô tả
      )
    );
  }

  /**
  * Tạo form option cho widget
  */
  function form( $instance ) {
    parent::form( $instance );

    //Biến tạo các giá trị mặc định trong form
    $default = array(
      'title' => 'Tiêu đề widget',
      'content' => 'Hãy nhập nội dung'
    );

    //Gộp các giá trị trong mảng $default vào biến $instance để nó trở thành các giá trị mặc định
    $instance = wp_parse_args( (array) $instance, $default);

    //Tạo biến riêng cho giá trị mặc định trong mảng $default
    $title = esc_attr( $instance['title'] );
    $content = esc_attr( $instance['content'] );

    //Hiển thị form trong option của widget
    echo '<p><label>Title</label>'.
      '<input class="widefat" type="text" name="' . $this->get_field_name('title') . '" value="' . $title . '" />'.
      '</p>';
    echo '<p><label>Nhập tiêu đề:</label>'.
      '<textarea class="widefat" name="' . $this->get_field_name('content') . '">' . $content . '</textarea>'.
      '</p>';
  }

  /**
  * save widget form
  */
  function update( $new_instance, $old_instance ) {
    parent::update( $new_instance, $old_instance );
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['content'] = strip_tags($new_instance['content']);
    return $instance;
  }

  /**
  * Show widget
  */
  function widget( $args, $instance ) {
    extract( $args );
    $title = apply_filters( 'widget_title', $instance['title'] );
    echo $before_widget;

    //In tiêu đề widget
    echo $before_title.$title.$after_title;

    // Nội dung trong widget
    echo $instance['content'];

    // Kết thúc nội dung trong widget
    echo $after_widget;
  }
}

/*
 * Khởi tạo widget item
 */
function create_custom_widget() {
  register_widget('custom_Widget');
}
add_action( 'widgets_init', 'create_custom_widget' );
