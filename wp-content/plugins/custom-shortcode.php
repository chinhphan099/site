<?php
/**
 * Plugin Name: Chinh Shortcode
 * Version: 1.0
 * Description: This is custom shotcodes
 * Author: Mr Chinh
 * Author URI: //facebook.com/chinhphan099
 * Help: https://www.youtube.com/watch?v=cwj6PBfLdJQ
*/

// Use: [youtube_video id="youtube_ID"]
add_shortcode( 'youtube_video', 'create_youtube_video' );
function create_youtube_video( $atts ) {
  extract(shortcode_atts(array(
    'id' => 'cwj6PBfLdJQ'
  ), $atts));
  return '<div class="video-container"><iframe src="//www.youtube.com/embed/' . $id . '" frameborder="0" allowfullscreen></iframe></div>';
}

// Use: [upper]Content[/upper]
add_shortcode('upper', 'uppercase');
function uppercase( $tag, $content ) {
  return '<span class="text-uppercase">'. $content .'</span>';
}

// Use: [related_posts limit="4" title="Bài viết liên quan"]
function related_posts_shortcode($atts) {
  extract(shortcode_atts(array(
    'limit' => '5',
    'title' => 'Related Post'
  ), $atts));

  global $wpdb, $post, $table_prefix;

  if ($post->ID) {
    $tags = wp_get_post_tags($post->ID);
    $tagsarray = array();
    foreach ($tags as $tag) {
      $tagsarray[] = $tag->term_id;
    }
    $tagslist = implode(',', $tagsarray);

    // Do the query
    $q = "SELECT p.*, count(tr.object_id) as count
      FROM $wpdb->term_taxonomy AS tt, $wpdb->term_relationships AS tr, $wpdb->posts AS p WHERE tt.taxonomy ='post_tag' AND tt.term_taxonomy_id = tr.term_taxonomy_id AND tr.object_id = p.ID AND tt.term_id IN ($tagslist) AND p.ID != $post->ID
        AND p.post_status = 'publish'
        AND p.post_date_gmt < NOW()
      GROUP BY tr.object_id
      ORDER BY count DESC, p.post_date_gmt DESC
      LIMIT $limit;";

    $related = $wpdb->get_results($q);
    if ( $related ) {
      $retval = '<div class="related-post"><h3>' . $title . '</h3><ul>';
      foreach($related as $r) {
        $retval .= '<li>';
        $retval .= '<div class="thumb"><img src="' . wptexturize(get_the_post_thumbnail_url()) . '"></div>';
        $retval .= '<div class="content"><a title="' . wptexturize($r->post_title) . '" href="'.get_permalink($r->ID).'">'.wptexturize($r->post_title).'</a></div>';
        $retval .= '</li>';
      }
      $retval .= '</ul></div>';
    }
    return $retval;
  }
  return;
}
add_shortcode('related_posts', 'related_posts_shortcode');
