<?php
/**
 * site Theme Customizer
 *
 * @package site
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function site_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'site_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'site_customize_partial_blogdescription',
		) );
	}

	// Settings Footer
	$wp_customize->add_section( 'site_footer' , array(
		'title'    => __( 'Footer Setting', 'site')
	));
	$wp_customize->add_setting( 'copyright' );
	$wp_customize->add_control(
		'site_footer',
		array(
			'label'    => __( 'Copyright', 'site' ),
			'section'  => 'site_footer',
			'settings' => 'copyright',
			'type'     => 'text'
		)
	);

	// Head notice
	$wp_customize->add_setting('head_notice');
	$wp_customize->add_control('head_notice',
		array(
			'label' => 'Head Notice',
			'section' => 'title_tagline',
			'settings' => 'head_notice',
			'type' => 'text'
		)
	);

	// Head notice
	$wp_customize->add_setting('phone_number');
	$wp_customize->add_control('phone_number',
		array(
			'label' => 'Phone Number',
			'section' => 'title_tagline',
			'settings' => 'phone_number',
			'type' => 'text'
		)
	);
}
add_action( 'customize_register', 'site_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function site_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function site_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function site_customize_preview_js() {
	wp_enqueue_script( 'site-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'site_customize_preview_js' );
