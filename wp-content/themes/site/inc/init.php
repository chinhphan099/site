<?php
function site_custom_script_style() {
	// wp_enqueue_style( 'iconfonts', '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', array(), '1.1', 'all');
	wp_enqueue_style( 'iconfonts', '//unpkg.com/ionicons@4.1.2/dist/css/ionicons.min.css', array(), '1.1', 'all');
	wp_enqueue_style( 'openSans', '//fonts.googleapis.com/css?family=Open+Sans:300,400,700,700i&amp;amp;subset=vietnamese', array(), '1.1', 'all');
	wp_enqueue_style( 'libs', get_template_directory_uri() . '/css/libs.css', array(), '1.1', 'all');
	wp_enqueue_style( 'plugins', get_template_directory_uri() . '/css/plugins.css', array(), '1.1', 'all');
	wp_enqueue_style( 'grid', get_template_directory_uri() . '/css/grid.css', array(), '1.1', 'all');
	wp_enqueue_style( 'style-custom', get_template_directory_uri() . '/css/style.css', array(), '1.1', 'all');

	if ( !is_admin() ) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.2.1.js', '3.2.1', false);
		wp_enqueue_script('jquery');
	}

	wp_enqueue_script( 'l10n', get_template_directory_uri() . '/js/l10n.js', array (), 1.1, true);
	wp_enqueue_script( 'libs', get_template_directory_uri() . '/js/libs.js', array (), 1.1, true);
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array (), 1.1, true);
	wp_localize_script( 'scripts', 'myAjax', array(
		'ajaxurl' => admin_url( '/admin-ajax.php' ),
	));
}
add_action( 'wp_enqueue_scripts', 'site_custom_script_style' );

// Add SVG to allowed file uploads
function allow_upload_SVG($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );

	return $file_types;
}
add_action('upload_mimes', 'allow_upload_SVG');

/*
 * Get url logo
 */
if(!function_exists('site_theme_logo')){
	function site_theme_logo(){
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		if ( $custom_logo_id ) {
			$url = wp_get_attachment_url( $custom_logo_id);
			return esc_url($url);
		}
		return '';
	}
}

// Login and Register
function redirect_login_page() {
	$login_page = home_url( '/my-account' );
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init', 'redirect_login_page');


// New
//add this within functions.php
function ajax_login_init(){
		add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
		add_action('init', 'ajax_login_init');
}

function ajax_login(){
		// First check the nonce, if it fails the function will break
		check_ajax_referer( 'ajax-login-nonce', 'security' );

		// Nonce is checked, get the POST data and sign user on
		$info = array();
		$info['user_login'] = $_POST['username'];
		$info['user_password'] = $_POST['password'];
		$info['remember'] = $_POST['remember'];
		$info['accountLink'] = $_POST['accountPageId'];
		$info['logoutLink'] = $_POST['logoutLink'];

		$user_signon = wp_signon( $info, false );
		if ( is_wp_error($user_signon) ){
			echo json_encode(array(
				'status'=>false,
				'message'=>__('Wrong username or password.')
			));
		} else {
			echo json_encode(array(
				'status'=>true,
				'userName'=>$user_signon->display_name,
				'isAdmin'=>$user_signon->caps['administrator'],
				'accountLink'=>$info['accountLink'],
				'logoutLink'=>get_page_link($info['logoutLink'])
			));
		}
		die();
}

add_filter( 'woocommerce_add_to_cart_fragments', function($fragments) {
	ob_start();
	?>
	<div class="mini-cart-content">
		<?php woocommerce_mini_cart(); ?>
	</div>
	<?php
	$fragments['span.cart-count'] = '<span class="cart-count">' . WC()->cart->get_cart_contents_count() . '</span>';
	$fragments['div.mini-cart-content'] = ob_get_clean();
	return $fragments;
});
