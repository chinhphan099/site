<?php
/**
 * The template for displaying all pages for WooCommerce
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package site
 */

get_header(); ?>

<main>
	<script>
		var $_headNotice = document.getElementsByClassName('head-notice'),
			$_html = document.getElementsByTagName('html')[0];
		if(!$_headNotice.length) {
			$_html.classList.add('no-head-notice');
		}
		else {
			document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
		}
	</script>
	<div class="product-page">
		<div class="container">
			<?php if ( !is_singular( 'product' ) ) : ?>
				<?php if ( is_active_sidebar( 'ecommerce-sidebar' ) ) : ?>
					<aside id="secondary" class="widget-area hidden-sm-down">
						<?php dynamic_sidebar( 'ecommerce-sidebar' ); ?>
					</aside>
				<?php endif; ?>
			<?php endif; ?>
			<div id="primary">
				<?php
					if ( have_posts() ) :
						woocommerce_content();
					endif;
				?>
			</div>
		</div>
	</div>
</main><!-- #main -->

<?php
get_footer();
