<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package site
 */

get_header(); ?>

<main class="post-page">
	<script>
		var $_headNotice = document.getElementsByClassName('head-notice'),
			$_html = document.getElementsByTagName('html')[0];
		if(!$_headNotice.length) {
			$_html.classList.add('no-head-notice');
		}
		else {
			document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
		}
	</script>
	<div class="container">
		<?php get_sidebar(); ?>
		<div id="primary">
			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'page' );
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
			?>
		</div>
	</div>
</main><!-- #main -->

<?php
get_footer();

