<?php
/*
  Template Name: Product Landing
*/

get_header(); ?>

<main>
  <script>
    var $_headNotice = document.getElementsByClassName('head-notice'),
      $_html = document.getElementsByTagName('html')[0];
    if(!$_headNotice.length) {
      $_html.classList.add('no-head-notice');
    }
    else {
      document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
    }
  </script>
  <div class="product-landing-page">
    <div class="container">
      <?php if ( !is_singular( 'product' ) ) : ?>
        <?php if ( is_active_sidebar( 'ecommerce-sidebar' ) ) : ?>
          <aside id="secondary" class="widget-area hidden-sm-down">
            <?php dynamic_sidebar( 'ecommerce-sidebar' ); ?>
          </aside>
        <?php endif; ?>
      <?php endif; ?>
      <div id="primary">
        <?php
          $args = array('parent' => 0 );
          $product_categories = get_terms('product_cat', $args);
          $count = count($product_categories);
          if ( $count > 0 ) {
            $i=0;
            foreach ( $product_categories as $product_category ) {
              $i++;
              $slug_cate = $product_category->slug;
              $name = $product_category->name;
              $category_link = get_term_link($slug_cate, 'product_cat');
              ?>
              <h2 class="main-title">
                <a href="<?php echo esc_url($category_link); ?>"><?php echo $name . $category_id ;?></a>
                <small><a href="<?php echo esc_url($category_link); ?>">Xem thêm <i class="ion ion-ios-arrow-dropright-circle"></i></a></small>
              </h2>
              <div class="product-wrap">
                <div class="product slider carousel-slider" data-cslider="{&quot;type&quot;: &quot;carousel&quot;, &quot;setPositionArrows&quot;: true, &quot;infinite&quot;: false}">
                  <div class="slick">
                    <?php
                      $feat_pro = new WP_Query(
                        array(
                          'post_type' => 'product',
                          'product_cat' => $slug_cate,
                          'posts_per_page' => 24,
                          'nopaging' => false
                        )
                      );
                      while ( $feat_pro->have_posts() ) :
                        $feat_pro->the_post();
                        global $product;
                        $regular_price = get_post_meta( get_the_ID(), '_regular_price', true );
                        $sale_price = get_post_meta( get_the_ID(), '_sale_price', true ) ? get_post_meta( get_the_ID(), '_sale_price', true ) : "";
                        $price = wc_format_sale_price($sale_price, $regular_price);
                        if(!!$sale_price) {
                          $price = wc_format_sale_price($sale_price, $regular_price);
                        }
                        else {
                          $price = wc_price($regular_price);
                        }
                      ?>
                      <div class="item">
                        <div class="product-item">
                          <a class="thumb" href="<?php the_permalink() ;?>"><?php the_post_thumbnail("medium",array( "title" => get_the_title(),"alt" => get_the_title() ));?></a>
                          <h4 class="product-title"><a href="<?php the_permalink() ;?>"><?php the_title() ;?></a></h4>
                          <span class="price"><?php echo $price; ?></span>
                        </div>
                      </div>
                      <?php endwhile; wp_reset_query(); ?>
                  </div>
              </div>
            </div>
        <?php } } ?>
      </div>
    </div>
  </div>
</main>

<?php
get_footer();
