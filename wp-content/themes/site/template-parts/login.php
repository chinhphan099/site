<?php
	global $wp;
	$current_url = home_url(add_query_arg(array(),$wp->request));
	$current_user = wp_get_current_user();
	$accountPageID = get_option('woocommerce_myaccount_page_id');
	$logoutLink = wp_logout_url($current_url);

	if ( 0 == $current_user->ID ): ?>
	<form id="topLoginForm" action="login" method="post" autocomplete="off">
		<input type="hidden" id="accountPageId" value="<?php echo $accountPageID; ?>">
		<input type="hidden" id="logoutLink" value="<?php echo $logoutLink; ?>">
		<input type="hidden" id="adminUrl" value="<?php echo get_admin_url(); ?>">
		<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		<div class="fieldset">
			<label class="label" for="top_username">Tên đăng nhập</label>
			<input id="top_username" type="text" name="top_username" autocomplete="off">
		</div>
		<div class="fieldset">
			<label class="label" for="top_password">Mật khẩu</label>
			<input id="top_password" type="password" name="top_password" autocomplete="off">
		</div>
		<div class="fieldset">
			<div class="fieldset">
				<div class="checkbox">
					<input name="top_remember" type="checkbox" id="top_remember">
					<label for="top_remember">Ghi nhớ đăng nhập</label><i class="icon-check"></i>
				</div>
			</div>
		</div>
		<div class="fieldset">
			<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Quên mật khẩu?</a>
			<a class="account-link" href="<?php echo get_permalink($accountPageID); ?>">Đăng ký</a>
			<p class="notice-message"></p>
			<button class="button btn-primary" type="submit">Đăng nhập</button>
		</div>
	</form>
<?php else: ?>
	<span class="name">Chào, <?php echo $current_user->display_name; ?></span>
	<div class="user-info">
		<a href="<?php echo get_permalink($accountPageID); ?>">Quản lý tài khoản</a>
		<a href="<?php echo $logoutLink; ?>">Đăng xuất</a>
	</div>
<?php endif; ?>
