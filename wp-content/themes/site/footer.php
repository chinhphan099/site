<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package site
 */
global $site;
?>
				<?php
					$copyright = $site['copyright'];
				?>
				<footer class="footer">
					<?php
						$phoneNumber = get_theme_mod('phone_number');
						if(!empty($phoneNumber)):
					?>
						<a href="tel:<?php echo $phoneNumber; ?>" rel="nofollow">
							<div class="mypage-alo-phone"><div class="animated infinite zoomIn mypage-alo-ph-circle"></div><div class="animated infinite pulse mypage-alo-ph-circle-fill"></div><div class="animated infinite tada mypage-alo-ph-img-circle"></div></div>
						</a>
					<?php endif ?>
					<button class="gotop" data-scrollto="{&quot;handle&quot;: &quot;body&quot;}"><i class="icon-arrow-up"></i></button>
					<div class="container">
						<?php $logo = site_theme_logo(); ?>
						<div class="flogo">
							<a href="<?php echo home_url();?>" title="<?php echo bloginfo( 'name' );?>">
								<img src="<?php echo $logo; ?>" alt="<?php echo bloginfo( 'name' );?>">
							</a>
						</div>
						<?php
							wp_nav_menu(array(
								'theme_location' => 'footer_links',
								'menu_class' => 'flinks'
							));
						?>
						<ul class="socials">
							<li><a href="//facebook.com"><i class="icon-facebook"></i></a></li>
							<li><a href="//twitter.com"><i class="icon-twitter"></i></a></li>
							<li><a href="//google.com"><i class="icon-googleplus"></i></a></li>
							<li><a href="//youtube.com"><i class="icon-youtube"></i></a></li>
						</ul>
						<p class="copyright"><?php echo get_theme_mod('copyright'); ?></p>
					</div>
				</footer>
			</div><!-- End .inner -->
		</div><!-- End .main-container -->
	</div><!-- End .wrapper -->
	<script>
		window.onload = function() {
			document.getElementsByTagName('html')[0].classList.add('window-loaded');
		}
	</script>
	<?php wp_footer(); ?>
</body>
</html>
