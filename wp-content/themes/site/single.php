<?php
/* Blog Detail Page */

get_header(); ?>

<main>
	<?php get_template_part( 'template-parts/hero-section', 'none' ); ?>
	<div class="blog-details">
		<div class="container">
			<?php get_sidebar(); ?>
			<div id="primary">
				<div class="post-detail">
					<div class="entry-header">
						<h1 class="post-title"><span><?php the_title(); ?></span></h1>
					</div>
					<div class="editor">
						<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
					</div>
				</div>

				<?php
					while ( have_posts() ) : the_post();
						the_post_navigation(array(
								'screen_reader_text' => 'Xem thêm'
							));
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					endwhile;
				?>
			</div>
		</div>
	</div>
</main><!-- #main -->

<?php
get_footer();
