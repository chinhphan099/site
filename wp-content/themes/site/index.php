<?php
/* Blog Landing Page */

get_header(); ?>

<main>
	<script>
		var $_headNotice = document.getElementsByClassName('head-notice'),
			$_html = document.getElementsByTagName('html')[0];
		if(!$_headNotice.length) {
			$_html.classList.add('no-head-notice');
		}
		else {
			document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
		}
	</script>
	<div class="blog-landing">
		<div class="container">
			<?php get_sidebar(); ?>
			<div id="primary">
				<?php get_template_part( 'template-parts/hero-section', 'none' ); ?>
				<div class="clearfix">
					<!-- <?php if (is_home()) : ?>
						<h1><?php single_post_title(); ?></h1>
					<?php endif; ?> -->
					<?php if (have_posts()) :
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content', get_post_format() );
							endwhile;
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif;
					?>
				</div>
				<!-- Pagination -->
				<?php
					if (have_posts()) {
						the_posts_navigation(array(
							'prev_text' => __( 'Older' ),
							'next_text' => __( 'Newer' )
						));
					}
				?>
				<!-- End Pagination -->
			</div>
		</div>
	</div>
</main>

<?php
get_footer();
