<?php
/*
  Template Name: FAQs
*/

get_header(); ?>

<main>
	<script>
		var $_headNotice = document.getElementsByClassName('head-notice'),
			$_html = document.getElementsByTagName('html')[0];
		if(!$_headNotice.length) {
			$_html.classList.add('no-head-notice');
		}
		else {
			document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
		}
	</script>
	<div class="faqs">
		<div class="container">
			<?php get_sidebar(); ?>
			<div id="primary">
				<?php if(have_rows('faqs')) : ?>
					<div class="accordion" data-collapse="{}">
						<?php
							while(have_rows('faqs')) : the_row();
								if(get_sub_field('faqs')) :
									$post_object = get_sub_field('faqs');
									$post = $post_object;
									$question = the_title('', '', false);
									setup_postdata( $post );
									$count++;
						?>
							<h2 class="acor-title" <?php if($count == 1) echo "data-init" ?> data-handle="<?php echo 'question-' . $count; ?>"><?php echo $question; ?> <i class="icon-arrow-dropdown"></i></h2>
							<div class="acor-content" data-content="<?php echo 'question-' . $count; ?>"><?php the_content(); ?></div>
						<?php
							endif;
							wp_reset_postdata();
							endwhile;
						?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</main>

<?php
get_footer();
