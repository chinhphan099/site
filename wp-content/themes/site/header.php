<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package site
 */
global $site;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, minimal-ui, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="wrapper">
		<div class="mini-cart-wrap">
			<i class="ion ion-ios-close-circle-outline"></i>
			<div class="inner">
				<div class="mini-cart-content"></div>
			</div>
		</div>
		<div class="main-container">
			<div class="dummy-layer"></div>
			<header class="header">
				<div class="header-wrap">
					<div class="header-outer">
						<?php
							$headNotice = get_theme_mod('head_notice');
							if(!empty($headNotice)):
						?>
							<div class="head-notice hidden-sm-down"><?php echo $headNotice; ?></div>
						<?php endif ?>
						<div class="main-header">
							<div class="header-inner">
								<!-- Quick Access -->
								<ul class="quick-access pull-left left-list">
									<!-- Hamburger Mobile -->
									<li class="hamburger-item hidden-lg-up">
										<a class="hamburger" href="javascript:;">
											<i class="ion ion-ios-menu"></i>
											<i class="ion ion-ios-close-circle-outline"></i>
										</a>
									</li>

									<!-- Search Desktop Form -->
									<li class="top-search-wrapper">
										<a href="javascript:;" class="search">
											<i class="ion-ios-search"></i>
											<i class="ion ion-ios-close-circle-outline"></i>
										</a>
										<div class="top-search-inner" data-validates="{&quot;isHideMessage&quot;: true}">
											<form role="search" id="desktopSeachFrm" method="get" class="search-form" action="<?php echo get_site_url(); ?>">
												<div class="fieldset">
													<div class="custom-input custom-search">
														<button class="button btn-primary" type="submit">
															<i class="ion-ios-search"></i>
														</button>
														<span><input type="search" required class="search-field" placeholder="Search …" value="" name="s"></span>
													</div>
												</div>
											</form>
										</div>
									</li>
									<?php
										$phoneNumber = get_theme_mod('phone_number');
										if(!empty($phoneNumber)):
									?>
										<li class="hidden-md-down phone-number"><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a></li>
									<?php endif ?>
								</ul>
								<ul class="quick-access pull-right right-list">
									<li class="mini-cart">
										<a href="<?php echo WC()->cart->get_cart_url(); ?>" data-minicart>
											<i class="ion ion-ios-cart"></i>
											<span class="cart-count">
												<?php echo WC()->cart->get_cart_contents_count(); ?>
											</span>
										</a>
									</li>

									<li class="account-control">
										<a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">
											<i class="ion ion-ios-contact"></i>
										</a>
										<div class="account-login hidden-sm-down" data-validates="{&quot;loadingOnSubmit&quot;: true, &quot;isHideMessage&quot;: true}">
											<?php get_template_part( 'template-parts/login', 'none' ); ?>
										</div>
									</li>
								</ul>
								<!-- /Quick Access -->

								<!-- Logo -->
								<?php
								$logo = site_theme_logo();
								if(is_front_page()): ?>
									<h1 class="logo">
										<a href="<?php echo home_url();?>" title="<?php echo bloginfo( 'name' );?>">
											<img src="<?php echo $logo; ?>" alt="<?php echo bloginfo( 'name' );?>">
										</a>
									</h1>
								<?php else: ?>
									<div class="logo">
										<a href="<?php echo home_url();?>" title="<?php echo bloginfo( 'name' );?>">
											<img src="<?php echo $logo; ?>" alt="<?php echo bloginfo( 'name' );?>">
										</a>
									</div>
								<?php endif; ?>
								<!-- /Logo -->
							</div>
						</div>

						<div class="navigation-wrap">
							<div class="main-navigation">
								<?php
									wp_nav_menu(array(
										'theme_location' => 'primary',
										'container' => 'nav',
										'container_class' => 'main-nav',
										'menu_class' => 'navigation'
									));
								?>
							</div>
							<!-- <script type="text/javascript">
								var nav = document.querySelector('.main-nav').cloneNode(true),
									innerElm = document.querySelector('.left-navigation .inner');
								innerElm.insertBefore(nav, innerElm.firstChild);
							</script> -->
						</div>
					</div>
				</div>
			</header>
			<div class="inner">
