<?php
/*
	Template Name: My Account
*/

get_header(); ?>

<main>
	<script>
		var $_headNotice = document.getElementsByClassName('head-notice'),
			$_html = document.getElementsByTagName('html')[0];
		if(!$_headNotice.length) {
			$_html.classList.add('no-head-notice');
		}
		else {
			document.write('<div class="head-notice hidden-md-up">' + $_headNotice[0].innerHTML + '</div>');
		}
	</script>
	<div class="my-account">
		<div class="container">
			<?php
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
			?>
		</div>
	</div>
</main>

<?php
get_footer();
