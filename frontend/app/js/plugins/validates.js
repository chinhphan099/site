/**
 *  @name validates
 *  @description Use jQuery Validation
 *  @author Chinh Phan
 *  @methods
 *    init
 *    reset
 */

;(function($, window, undefined) {
	'use strict';

	var pluginName = 'validates',
		getRules = function(formId) {
			var rules = {};
			switch (formId) {
				case 'topLoginForm':
					rules = $.extend(rules, {
						top_username: {
							required: true
						},
						top_password: {
							required: true
						}
					});
					break;
				case 'desktopSeachFrm':
					rules = $.extend(rules, {
						s: {
							required: true,
							alpha: true
						}
					});
					break;
				default:
			}
			return rules;
		},
		messages = function(formId) {
			var messages = {};
			switch (formId) {
				case 'topLoginForm':
					messages = $.extend(messages, {
					});
					break;
				default:
			}
			return messages;
		},
		errorPlacement = function(error, element) {
			if(!this.isHideMessage) {
				if ($(element).is('select')) {
					error.insertAfter(element.closest('.custom-select'));
				}
				else if ($(element).closest('.custom-input').length) {
					error.insertAfter(element.closest('.custom-input'));
				}
				else if ($(element).closest('.radio-list').length) {
					error.insertAfter(element.closest('.radio-list'));
				}
				else if ($(element).closest('.checkbox-list').length) {
					error.insertAfter(element.closest('.checkbox-list'));
				}
				else if($(element).attr('type') === 'date') {
					error.insertAfter(element.closest('.custom-date'));
					return false;
				}
				else if($(element).attr('type') === 'file') {
					error.insertAfter(element.closest('.custom-file'));
					return false;
				}
				else if($(element).is(':checkbox')) {
					error.insertAfter(element.closest('.checkbox'));
				}
				else if($(element).is(':radio')) {
					error.insertAfter(element.closest('.radio'));
				}
				else {
					error.insertAfter(element);
				}
			}
		},
		submitHandler = function(form, submitButton) {
			console.log(submitButton);
			switch (this.formId) {
				case 'topLoginForm':
					var dataPost = {
						'action': 'ajaxlogin',
						'security': $('#security', '#' + this.formId).val(),
						'username': $('#top_username', '#' + this.formId).val(),
						'password': $('#top_password', '#' + this.formId).val(),
						'remember': $('#top_remember', '#' + this.formId).prop('checked'),
						'accountPageId': $('#accountPageId', '#' + this.formId).val(),
						'logoutLink': $('#logoutLink', '#' + this.formId).val()
					};
					UTILS.callAjax('POST', myAjax.ajaxurl, dataPost, this.loadingOnSubmit).done(function(response) {
						var data = $.parseJSON(response);
						if(data.status) {
							if(data.isAdmin) {
								window.location.href = $('#adminUrl').val();
								return;
							}
							else {
								var html = '<span class="name">Hi, ' + data.userName + '</span>' +
									'<div class="user-info">' +
										'<a href="' + data.accountLink + '">My account</a>' +
										'<a href="' + data.logoutLink + '">Logout</a>' +
									'</div>';
								$('.account-login').html(html);
							}
						}
						else {
							$('.notice-message').html(data.message);
						}
					});
					break;
				default:
					form.submit();
					return false;
			}
			return false;
		};

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options, this.element.data(pluginName));
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var that = this;
			this.formId = this.element.find('form').attr('id');
			this.isHideMessage = this.options.isHideMessage;
			this.loadingOnSubmit = this.options.loadingOnSubmit;

			this.validator = $('#' + this.formId).validate({
				rules: getRules.call(that, that.formId),
				messages: messages.call(that, that.formId),
				errorElement: 'p',
				highlight: function(element) {
					$(element).addClass('error').closest('.fieldset').addClass('errors');
				},
				unhighlight: function(element) {
					$(element).removeClass('error');
					if(!$(element).closest('.fieldset').find(that.options.groupCls).hasClass('error')) {
						$(element).removeClass('error').closest('.fieldset').removeClass('errors');
					}
				},
				invalidHandler: function(event, validator) {
					var errors = validator.numberOfInvalids();
					console.log(errors);
				},
				errorPlacement: function(error, element) {
					errorPlacement.call(that, error, element);
				},
				submitHandler: function(form) {
					submitHandler.call(that, form, this.submitButton);
				}
			});
		},
		reset: function() {
			this.validator.resetForm();
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			}
		});
	};

	$.fn[pluginName].defaults = {
		isHideMessage: false,
		loadingOnSubmit: false,
		groupCls: '.valid-group'
	};

	$(function() {
		$('[data-' + pluginName + ']')[pluginName]();
	});

}(jQuery, window));
