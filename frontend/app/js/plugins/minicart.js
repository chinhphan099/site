/**
 *  @name minicart
 *  @description Use fancybox
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'minicart',
    body = $('body'),
    html = $('html');

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options, this.element.data(pluginName));
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.vars = {
        openCls: this.options.openCls
      };
      this.listener();
    },
    listener: function() {
      var that = this;
      this.element.off('click.showMiniCart').on('click.showMiniCart', function(e) {
        e.stopPropagation();
        e.preventDefault();
        if(!html.hasClass(that.vars.openCls)) {
          that.showMiniCart();
        }
        else {
          that.hideMiniCart();
        }
      });

      body.off('click.hideMiniCart').on('click.hideMiniCart', function(e) {
        if(!$(e.target).closest('.mini-cart-wrap').length || $(e.target).hasClass('ion-ios-close-empty')) {
          that.hideMiniCart();
        }
      });
    },
    showMiniCart: function() {
      html.addClass(this.vars.openCls);
    },
    hideMiniCart: function() {
      html.removeClass(this.vars.openCls);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    openCls: 'open-mini-cart'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
