/**
 *  @name popup
 *  @description Use fancybox
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'popup';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options, this.element.data(pluginName));
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.vars = {
        handle: $(this.options.handle)
      };

      this.defineOption();
      this.listener();
    },
    defineOption: function() {
      this.vars.config = {
        type: this.options.type ? this.options.type : 'inline',
        autoFocus: false,
        backFocus: false,
        trapFocus: false,
        beforeLoad: this.beforeLoad,
        beforeClose: this.beforeClose
      };
    },
    listener: function() {
      var that = this;
      this.vars.handle.off('click.open' + pluginName).on('click.open' + pluginName, function(e) {
        e.preventDefault();
        $.fancybox.open(that.vars.handle, that.vars.config, that.vars.handle.index(this));
        return false;
      });
    },
    beforeLoad: function() {
      $('input', '#login-popup').val('').attr('value', '');
      $('.notice-message', '#login-popup').remove();
      $('#SendRes', '#login-popup').removeAttr('disabled').text('Send');
      $('.popup-content', '#login-popup').show();
      $('.step-success', '#login-popup').remove();
    },
    beforeClose: function() {
      $('[data-validates]', '.popup').validates('reset');
    },
    closePopup: function() {
      $.fancybox.close('all');
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {};

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
