var UTILS = (function($, window, undefined) {
	'use strict';

	var html = $('html'),
		isTouchDevice = 'ontouchstart' in document.documentElement,
		isAndroidMobile = /(android)/i.test(navigator.userAgent) && /(mobile)/i.test(navigator.userAgent);

	// Private functions
	function wowjsFct() {
		var wowjs = new WOW({
			boxClass: 'site',
			offset: 50
		});
		wowjs.init();
	}
	function customSelect() {
		$('.wrapper select').selectric();
	}

	function customValid() {
		// Change default Valid message
		$.extend($.validator.messages, {
			required: L10n.validateMess.required
		});

		// Valid Email
		$.validator.addMethod('cemail', function(value, element) {
			if (element.value !== '') {
				return this.optional(element) || /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.) {2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i.test(value);
			} else {
				return true;
			}
		}, L10n.validateMess.cemail);

		$.validator.addMethod('alpha', function(value, element) {
			if (element.value !== '') {
				return this.optional(element) || /^[a-zA-Z0-9\s]*$/i.test(value);
			} else {
				return true;
			}
		}, L10n.validateMess.alpha);
	}

	function checkDevice() {
		if(isTouchDevice) {
			html.addClass('touch');
		}
		else {
			html.addClass('no-touch'); // No need to check emulator on/off :D
		}
	}
	// End Private Functions

	var checkScrollWindow = function() {
		var body = $('body');

		if($(window).scrollTop() >= 100 && !body.hasClass('scrolled')) {
			body.addClass('scrolled');
		}
		else if($(window).scrollTop() < 100 && body.hasClass('scrolled')) {
			body.removeClass('scrolled');
		}
	};

	var globalFct = function() {
		customValid();
		wowjsFct();
		customSelect();
		checkDevice();
		checkScrollWindow();
		$(window).on('scroll.scrollWindow', function() {
			checkScrollWindow();
		});
	};
	// End Global Functions

	// Public functions
	var callAjax = function(method, url, postData, loadingOnSubmit, submitButton) {
		var deferred = $.Deferred();

		$.ajax({
			method: method,
			url: url,
			data: !!postData ? postData : null,
			beforeSend: function () {
				if(typeof loadingOnSubmit === 'string') { // Ex: loadingOnSubmit = 'Sending...'
					console.log(loadingOnSubmit);
					$(submitButton).html(loadingOnSubmit).prop('disabled', true);
				}
				else if(loadingOnSubmit) {// loadingOnSubmit = true
					console.log('Loading...');
				}
			},
			success: function (data) {
				deferred.resolve([data]);
			},
			complete: function() {
			},
			error: function (error) {
				deferred.reject(error);
			}
		});

		return deferred.promise();
	};
	// End Public functions

	return {
		globalFct: globalFct,
		isTouchDevice: isTouchDevice,
		isAndroidMobile: isAndroidMobile,
		callAjax: callAjax
	};

})(jQuery, window);

jQuery(function() {
	UTILS.globalFct();
});
