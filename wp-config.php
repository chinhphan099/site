<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// Chinh
// Update wordpress, update plugins
define('SAVEQUERIES', true);
define('FS_METHOD','direct');
// End Chinh

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v0X22IYB1{~0%c4s JP62)pywG`<ZI%#^4C_BzMosVIwNNNoa?.]6G<|,KxG5FWG');
define('SECURE_AUTH_KEY',  'g~qN~u__<+t2JXSOl~e,i/y7%n~&:p]J+_+0RD=-~IEH9NDnZ/Iy8u)%soYdWkFe');
define('LOGGED_IN_KEY',    'i=lvcco@K&0j@i_$4ET)WV;_Op*7;$G]H${hiUkFu3)}!|6K%2C%pSzBvnp}@=Sr');
define('NONCE_KEY',        'p~x3R,55*GZ{/c#3sQ.m#t qPa[AJSdI;]tj%]nCH,|c<|&p9]PVB=1TF8rCMmJ3');
define('AUTH_SALT',        'pY,TM]gDMUW%<(GO^IGNE$98&5OD%*KEqou<(et}j.~h]+yl_o1tL{zkf$Y(fKPT');
define('SECURE_AUTH_SALT', '$Hx5Sml333VN?$NzI: IeEZ4e l*|k&Dawy*=~1o-D.SlY^fEq.:9vAd1rabVV[j');
define('LOGGED_IN_SALT',   '5i481P]r1*L725$|J+[WllSap>bb~zBD(u_ )#nbR}dbzQ6kH0kEx0PG1bDkk[6)');
define('NONCE_SALT',       '@&z/Ik/U:Ggve]4 $p`eT%0mGB{aF1x.w<I[=R+[[_oT`th>fycm]Q`!JL$4%<D&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'site_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
